# Конструктор формы
Основу тестового задания составляет [React Boilerplate](https://github.com/react-boilerplate/react-boilerplate).

Для запуска необходимо:

1. Установить на компьютер Node.js версии 8 или выше.
2. В консоли выполнить команду `git clone git@bitbucket.org:terbiy/general-electric-testing-assignment.git` для загрузки через `SSH` или аналогичную ей `git clone https://terbiy@bitbucket.org/terbiy/general-electric-testing-assignment.git` для загрузки через `HTTPS`.
3. В консоли из папки проекта произвести команду `npm install` для установки зависимостей.
4. В консоли из папки проекта выполнить команду `npm run start` для запуска приложения.
5. Перейти по адресу [localhost:3000](http://localhost:3000/) или другому, указанному в выводе консоли.
