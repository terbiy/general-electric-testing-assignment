import { injectGlobal, css } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  input:focus {
    outline: 0;
  }
`;

export const doubleBordered = css`
  border-top: 1px solid lightgrey;
  border-bottom: 1px solid lightgrey;
`;

export const colors = {
  main: 'wheat',
  mainLight: '#fbf2e0',
  complementary: '#b3caf5',
  complementaryLight: '#e0e9fb',
};

export const sizes = {
  verySmall: '480px',
  small: '768px',
  medium: '992px',
};

export const airy = css`
  padding: 16px;
  background-color: ${colors.complementaryLight};
`;
