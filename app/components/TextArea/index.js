/**
 *
 * TextArea
 *
 */

import styled from 'styled-components';
import { airy } from 'global-styles';

export default styled.textarea`
  width: 100%;
  height: 100px;
  ${airy};
`;
