/**
 *
 * Arrows
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Up from 'components/Up';
import Down from 'components/Down';

const ArrowsWrapper = styled.div`
  flex-basis: 5%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

function Arrows(props) {
  return (
    <ArrowsWrapper>
      <Up onClick={props.onMoveBlockUp} />
      <Down onClick={props.onMoveBlockDown} />
    </ArrowsWrapper>
  );
}

Arrows.propTypes = {
  onMoveBlockUp: PropTypes.func,
  onMoveBlockDown: PropTypes.func,
};

export default Arrows;
