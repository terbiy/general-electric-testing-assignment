/**
 *
 * Error
 *
 */

import styled from 'styled-components';

export default styled.span`
  display: block;
  font-size: 12px;
  line-height: 18px;
  color: darkred;
`;
