/**
 *
 * FormDisplayButton
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import StyledButton from 'components/StyledButton';

import messages from './messages';

function FormDisplayButton(props) {
  return (
    <StyledButton onClick={props.onDisplayForm} disabled={props.disabled}>
      <FormattedMessage {...messages.showForm} />
    </StyledButton>
  );
}

FormDisplayButton.propTypes = {
  onDisplayForm: PropTypes.func,
  disabled: PropTypes.bool,
};

export default FormDisplayButton;
