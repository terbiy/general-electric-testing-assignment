/*
 * FormDisplayButton Messages
 *
 * This contains all the text for the FormDisplayButton component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  showForm: {
    id: 'formConstructor.components.FormDisplayButton.showForm',
    defaultMessage: 'Показать форму',
  },
});
