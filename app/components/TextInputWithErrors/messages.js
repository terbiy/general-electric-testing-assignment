/*
 * FormConstructorPage Messages
 *
 * This contains all the text for the FormConstructorPage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  errorDuplicate: {
    id: 'formConstructor.components.TextInputWithErrors.errorDuplicate',
    defaultMessage: 'Текст дублируется',
  },
  errorEmpty: {
    id: 'formConstructor.components.TextInputWithErrors.errorEmpty',
    defaultMessage: 'Обязательно к заполнению',
  },
});
