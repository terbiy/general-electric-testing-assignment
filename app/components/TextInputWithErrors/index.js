/**
 *
 * InputWithErrors
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

import { sizes } from 'global-styles';

import Error from 'components/Error';
import TextInput from 'components/TextInput';

import messages from './messages';

function getErrors(errors) {
  if (errors.duplicate) {
    return (
      <Error key="duplicate">
        <FormattedMessage {...messages.errorDuplicate} />
      </Error>
    );
  }

  return null;
}

const Wrapper = styled.label`
  display: block;
  position: relative;
  height: 42px;
  margin-bottom: 12px;

  &.big {
    height: 62px;

    input {
      font-size: 20px;

      @media only screen and (max-width: ${sizes.small}) {
        font-size: 16px;
      }
    }
  }

  @media only screen and (max-width: ${sizes.small}) {
    font-size: 12px;
  }
`;

function TextInputWithErrors({
  id = '',
  value,
  errors,
  placeholder,
  big,
  onChange,
}) {
  const { empty } = errors;

  return (
    <Wrapper key={id} className={big ? 'big' : ''}>
      <TextInput
        type="text"
        value={value}
        placeholder={placeholder}
        onChange={onChange}
        className={empty ? 'erroneous' : ''}
      />
      {getErrors(errors)}
    </Wrapper>
  );
}

TextInputWithErrors.propTypes = {
  id: PropTypes.string,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  errors: PropTypes.shape({
    empty: PropTypes.bool,
    duplicate: PropTypes.bool,
  }),
  big: PropTypes.bool,
  onChange: PropTypes.func,
};

export default TextInputWithErrors;
