/**
 *
 * Up
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

function Up(props) {
  const Base = styled.span`
    color: grey;
    cursor: pointer;
  `;

  return <Base onClick={props.onClick}>▲</Base>;
}

Up.propTypes = {
  onClick: PropTypes.func,
};

export default Up;
