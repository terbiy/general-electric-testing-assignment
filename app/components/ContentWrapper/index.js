/**
 *
 * ContentWrapper
 *
 */

import styled from 'styled-components';

export default styled.div`
  width: 90%;
  max-width: 1024px;
  margin: auto;
`;
