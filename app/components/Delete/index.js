/**
 *
 * Delete
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Cross = styled.div`
  color: grey;
  font-size: 1em;
`;

function Delete(props) {
  return <Cross onClick={props.onDelete}>✕</Cross>;
}

Delete.propTypes = {
  onDelete: PropTypes.func,
};

export default Delete;
