/**
 *
 * FormBlockWrapper
 *
 */

import styled from 'styled-components/';
import { doubleBordered } from 'global-styles';

export default styled.section`
  width: 100%;
  margin: 0 0 20px;
  display: flex;
  padding: 20px 0;
  ${doubleBordered};
`;
