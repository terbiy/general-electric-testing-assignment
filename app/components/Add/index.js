/**
 *
 * Add
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Plus = styled.div`
  color: grey;
  font-size: 2em;
`;

function Add(props) {
  return <Plus onClick={props.onAdd}>+</Plus>;
}

Add.propTypes = {
  onAdd: PropTypes.func,
};

export default Add;
