/**
 *
 * TextInput
 *
 */

import styled from 'styled-components';
import { airy } from 'global-styles';

export default styled.input`
  width: 100%;

  &.emphasis {
    border-bottom: 1px solid lightgrey;

    &[type='text'] {
      border-bottom: 0;
      ${airy};
    }
  }

  &.erroneous {
    border-bottom: 1px solid red;
  }
`;
