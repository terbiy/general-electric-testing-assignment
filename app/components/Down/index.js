/**
 *
 * Down
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

function Down(props) {
  const Base = styled.span`
    color: grey;
    cursor: pointer;
  `;

  return <Base onClick={props.onClick}>▼</Base>;
}

Down.propTypes = {
  onClick: PropTypes.func,
};

export default Down;
