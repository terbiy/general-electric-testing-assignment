/**
 *
 * H2
 *
 */

import styled from 'styled-components/';

export default styled.h2`
  font-size: 20px;
  font-weight: normal;
  line-height: 26px;
`;
