/**
 *
 * StylelessUl
 *
 */

import styled from 'styled-components/';
import { sizes, doubleBordered } from 'global-styles';

export default styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0;

  &.horizontal {
    @media only screen and (max-width: ${sizes.small}) {
      display: flex;
      justify-content: space-between;
      flex-wrap: wrap;

      > li {
        margin: 8px 16px;
        ${doubleBordered};
      }
    }
  }
`;
