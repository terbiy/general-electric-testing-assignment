/**
 *
 * StyledButton
 *
 */

import styled from 'styled-components/';
import { colors, sizes } from 'global-styles';

export default styled.button`
  height: 7.5vh;
  padding: 0 20px;
  box-sizing: border-box;
  background-color: ${colors.main};

  &.submit {
    background-color: ${colors.complementary};

    &:disabled {
      background-color: ${colors.complementaryLight};
    }
  }

  &:disabled {
    background-color: ${colors.mainLight};
  }

  @media only screen and (max-width: ${sizes.verySmall}) {
    display: block;
    margin: auto;
  }
`;
