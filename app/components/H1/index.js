/**
 *
 * H1
 *
 */

import styled from 'styled-components/';

export default styled.h1`
  font-size: 32px;
  font-weight: normal;
  margin: 32px 0;
  line-height: 39px;
`;
