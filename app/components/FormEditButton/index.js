/**
 *
 * FormEditButton
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import StyledButton from 'components/StyledButton';

import messages from './messages';

function FormEditButton(props) {
  return (
    <StyledButton onClick={props.onFormEdit}>
      <FormattedMessage {...messages.editForm} />
    </StyledButton>
  );
}

FormEditButton.propTypes = {
  onFormEdit: PropTypes.func,
};

export default FormEditButton;
