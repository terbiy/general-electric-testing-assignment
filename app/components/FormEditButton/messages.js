/*
 * FormEditButton Messages
 *
 * This contains all the text for the FormEditButton component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  editForm: {
    id: 'formConstructor.components.FormEditButton.editForm',
    defaultMessage: 'Редактировать форму',
  },
});
