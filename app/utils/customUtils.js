import { OPTIONS } from 'globalConstants';

function getArrayFromOrderedMap(orderedMap) {
  return Array.from(orderedMap.entries()).map(([id, data]) => ({ id, data }));
}

export function getArrayFormOrderMapWithOptions(orderMap) {
  return getArrayFromOrderedMap(orderMap).map(({ id, data }) => {
    const itemData = {
      id,
      data: data.toJS(),
    };

    const options = data.get(OPTIONS);

    itemData.data.options =
      options &&
      getArrayFromOrderedMap(options).map(option => ({
        ...option,
        data: option.data.toJS ? option.data.toJS() : option.data,
      }));

    return itemData;
  });
}
