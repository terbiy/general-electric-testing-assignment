/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import FormConstructorPage from 'containers/FormConstructorPage/Loadable';
import FormDisplayPage from 'containers/FormDisplayPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

import ContentWrapper from 'components/ContentWrapper';

import { FORM_CONSTRUCTOR_ROUTE, FORM_DISPLAY_ROUTE } from 'globalConstants';

export default function App() {
  return (
    <ContentWrapper>
      <Switch>
        <Route
          exact
          path={FORM_CONSTRUCTOR_ROUTE}
          component={FormConstructorPage}
        />
        <Route exact path={FORM_DISPLAY_ROUTE} component={FormDisplayPage} />
        <Route component={NotFoundPage} />
      </Switch>
    </ContentWrapper>
  );
}
