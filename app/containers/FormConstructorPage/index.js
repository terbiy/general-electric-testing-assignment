/**
 *
 * FormConstructorPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import { push } from 'react-router-redux';
import styled from 'styled-components';

import { sizes } from 'global-styles';
import { FORM_DISPLAY_ROUTE } from 'globalConstants';
import FormDisplayButton from 'components/FormDisplayButton';

import {
  changeFormName,
  addFormElementSettingsBlock,
  deleteFormElementSettingsBlock,
  changeBlockName,
  changeBlockNecessity,
  addOption,
  changeOption,
  deleteOption,
  moveBlockUp,
  moveBlockDown,
} from './actions';
import {
  selectFormName,
  selectFormValidity,
  selectFormElements,
} from './selectors';
import reducer from './reducer';
import FormName from './FormName';
import FormElementSettingsBlock from './FormElementSettingsBlock';
import FormElementAddingBlock from './FormElementAddingBlock';

const FormWrapper = styled.div`
  display: flex;
  height: 92.5vh;

  @media only screen and (max-width: ${sizes.small}) {
    display: block;
  }
`;

const Form = styled.form`
  flex-basis: 55%;
  margin-right: 5%;
  padding-right: 5%;
  overflow: auto;
`;

/* eslint-disable react/prefer-stateless-function */
export class FormConstructorPage extends React.PureComponent {
  render() {
    return (
      <div>
        <FormDisplayButton
          onDisplayForm={this.props.onDisplayForm}
          disabled={this.formIsInvalidOrEmpty()}
        />
        <FormWrapper>
          <Form onSubmit={this.onSubmitForm}>
            <FormName
              formName={this.props.formName}
              onChangeFormName={this.props.onChangeFormName}
            />
            {this.getFormElementSettingsBlocks()}
          </Form>
          <FormElementAddingBlock
            onAddBlock={this.props.onAddFormElementSettingsBlock}
          />
        </FormWrapper>
      </div>
    );
  }

  formIsInvalidOrEmpty() {
    return !this.props.formIsValid || this.props.formElements.length === 0;
  }

  getFormElementSettingsBlocks() {
    return this.props.formElements.map(formElement => {
      const { id, data } = formElement;

      return (
        <FormElementSettingsBlock
          key={id}
          data={data}
          onDeleteBlock={() => this.props.onDeleteFormElementSettingsBlock(id)}
          onChangeBlockName={event =>
            this.props.onChangeBlockName(id, event.target.value)
          }
          onChangeBlockNecessity={() => this.props.onChangeBlockNecessity(id)}
          onMoveBlockUp={() => this.props.onMoveBlockUp(id)}
          onMoveBlockDown={() => this.props.onMoveBlockDown(id)}
          onAddOption={() => this.props.onAddOption(id)}
          onChangeOption={(optionId, value) =>
            this.props.onChangeOption(id, optionId, value)
          }
          onDeleteOption={optionId => this.props.onDeleteOption(id, optionId)}
        />
      );
    });
  }

  onSubmitForm(event) {
    if (event && event.preventDefault) {
      event.preventDefault();
    }
  }
}

FormConstructorPage.propTypes = {
  formName: PropTypes.string,
  formIsValid: PropTypes.bool,
  formElements: PropTypes.array,
  onChangeFormName: PropTypes.func,
  onAddFormElementSettingsBlock: PropTypes.func,
  onDeleteFormElementSettingsBlock: PropTypes.func,
  onChangeBlockName: PropTypes.func,
  onChangeBlockNecessity: PropTypes.func,
  onMoveBlockUp: PropTypes.func,
  onMoveBlockDown: PropTypes.func,
  onAddOption: PropTypes.func,
  onChangeOption: PropTypes.func,
  onDeleteOption: PropTypes.func,
  onDisplayForm: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  formName: selectFormName,
  formIsValid: selectFormValidity,
  formElements: selectFormElements,
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeFormName: event => dispatch(changeFormName(event.target.value)),
    onAddFormElementSettingsBlock: payload =>
      dispatch(addFormElementSettingsBlock(payload)),
    onDeleteFormElementSettingsBlock: id =>
      dispatch(deleteFormElementSettingsBlock(id)),
    onChangeBlockName: (blockId, name) =>
      dispatch(changeBlockName(blockId, name)),
    onChangeBlockNecessity: blockId => dispatch(changeBlockNecessity(blockId)),
    onMoveBlockUp: blockId => dispatch(moveBlockUp(blockId)),
    onMoveBlockDown: blockId => dispatch(moveBlockDown(blockId)),
    onAddOption: blockId => dispatch(addOption(blockId)),
    onChangeOption: (blockId, optionId, value) =>
      dispatch(changeOption(blockId, optionId, value)),
    onDeleteOption: (blockId, optionId) =>
      dispatch(deleteOption(blockId, optionId)),
    onDisplayForm: () => dispatch(push(FORM_DISPLAY_ROUTE)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'formConstructorPage', reducer });

export default compose(
  withReducer,
  withConnect,
)(FormConstructorPage);
