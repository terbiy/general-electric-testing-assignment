/*
 * FormConstructorPage Messages
 *
 * This contains all the text for the FormConstructorPage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  formName: {
    id: 'formConstructor.containers.FormConstructorPage.formName',
    defaultMessage: 'Название формы',
  },
  typeFormName: {
    id: 'formConstructor.containers.FormConstructorPage.typeFormName',
    defaultMessage: 'Введите название опросника',
  },
  typeBlockName: {
    id: 'formConstructor.containers.FormConstructorPage.typeBlockName',
    defaultMessage: 'Введите вопрос',
  },
  typeOption: {
    id: 'formConstructor.containers.FormConstructorPage.typeOption',
    defaultMessage: 'Введите вариант выбора',
  },
  add: {
    id: 'formConstructor.containers.FormConstructorPage.add',
    defaultMessage: 'Добавить:',
  },
  makeRequired: {
    id: 'formConstructor.containers.FormConstructorPage.makeRequired',
    defaultMessage: 'обязательный вопрос',
  },
  text: {
    id: 'formConstructor.containers.FormConstructorPage.text',
    defaultMessage: 'Строка',
  },
  textarea: {
    id: 'formConstructor.containers.FormConstructorPage.textarea',
    defaultMessage: 'Текстовое поле',
  },
  checkbox: {
    id: 'formConstructor.containers.FormConstructorPage.checkbox',
    defaultMessage: 'Несколько на выбор',
  },
  radio: {
    id: 'formConstructor.containers.FormConstructorPage.radio',
    defaultMessage: 'Один на выбор',
  },
  select: {
    id: 'formConstructor.containers.FormConstructorPage.select',
    defaultMessage: 'Выпадающий список',
  },
  file: {
    id: 'formConstructor.containers.FormConstructorPage.file',
    defaultMessage: 'Файл',
  },
});
