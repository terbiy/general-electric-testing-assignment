/*
 *
 * FormConstructorPage actions
 *
 */

import {
  CHANGE_FORM_NAME,
  ADD_FORM_ELEMENT_SETTINGS_BLOCK,
  DELETE_FORM_ELEMENT_SETTINGS_BLOCK,
  CHANGE_BLOCK_NAME,
  CHANGE_BLOCK_NECESSITY,
  CHANGE_OPTION,
  ADD_OPTION,
  DELETE_OPTION,
  MOVE_BLOCK_UP,
  MOVE_BLOCK_DOWN,
} from './constants';

export function changeFormName(name) {
  return {
    type: CHANGE_FORM_NAME,
    payload: {
      name,
    },
  };
}

export function addFormElementSettingsBlock(payload) {
  return {
    type: ADD_FORM_ELEMENT_SETTINGS_BLOCK,
    payload,
  };
}

export function deleteFormElementSettingsBlock(id) {
  return {
    type: DELETE_FORM_ELEMENT_SETTINGS_BLOCK,
    payload: {
      id,
    },
  };
}

export function changeBlockName(blockId, name) {
  return {
    type: CHANGE_BLOCK_NAME,
    payload: {
      blockId,
      name,
    },
  };
}

export function changeBlockNecessity(blockId) {
  return {
    type: CHANGE_BLOCK_NECESSITY,
    payload: {
      blockId,
    },
  };
}

export function moveBlockUp(blockId) {
  return {
    type: MOVE_BLOCK_UP,
    payload: {
      blockId,
    },
  };
}

export function moveBlockDown(blockId) {
  return {
    type: MOVE_BLOCK_DOWN,
    payload: {
      blockId,
    },
  };
}

export function changeOption(blockId, optionId, value) {
  return {
    type: CHANGE_OPTION,
    payload: {
      blockId,
      optionId,
      value,
    },
  };
}

export function addOption(blockId) {
  return {
    type: ADD_OPTION,
    payload: {
      blockId,
    },
  };
}

export function deleteOption(blockId, optionId) {
  return {
    type: DELETE_OPTION,
    payload: {
      blockId,
      optionId,
    },
  };
}
