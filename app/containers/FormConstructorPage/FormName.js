import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

import { sizes } from 'global-styles';

import messages from './messages';

const NameInput = styled.input`
  font-size: 32px;
  width: 100%;
  margin: 1em 0;

  @media only screen and (max-width: ${sizes.medium}) {
    font-size: 24px;
  }

  @media only screen and (max-width: ${sizes.small}) {
    font-size: 16px;
  }
`;

function FormName(props) {
  return (
    <label>
      <FormattedMessage {...messages.typeFormName}>
        {typeFormName => (
          <NameInput
            type="text"
            placeholder={typeFormName}
            value={props.formName}
            onChange={props.onChangeFormName}
          />
        )}
      </FormattedMessage>
    </label>
  );
}

FormName.propTypes = {
  formName: PropTypes.string,
  onChangeFormName: PropTypes.func,
};

export default FormName;
