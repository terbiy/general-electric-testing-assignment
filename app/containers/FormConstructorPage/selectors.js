import { createSelector } from 'reselect';
import { FORM_ELEMENTS, VALID } from 'globalConstants';
import { getArrayFormOrderMapWithOptions } from 'utils/customUtils';

import { initialState } from './reducer';
import { FORM_NAME } from './constants';

const selectFormConstructorPage = state =>
  state.get('formConstructorPage', initialState);

const selectFormName = createSelector(
  selectFormConstructorPage,
  formConstructorPageState => formConstructorPageState.get(FORM_NAME),
);

const selectFormValidity = createSelector(
  selectFormConstructorPage,
  formConstructorPageState => formConstructorPageState.get(VALID),
);

const selectFormElements = createSelector(
  selectFormConstructorPage,
  formConstructorPageState =>
    getArrayFormOrderMapWithOptions(
      formConstructorPageState.get(FORM_ELEMENTS),
    ),
);

export {
  selectFormConstructorPage,
  selectFormName,
  selectFormValidity,
  selectFormElements,
};
