/*
 *
 * FormConstructorPage reducer
 *
 */

import { fromJS, OrderedMap } from 'immutable';
import uuidv4 from 'uuid/v4';
import {
  FORM_ELEMENTS,
  VALID,
  OPTIONS,
  VALUE,
  REQUIRED,
} from 'globalConstants';
import {
  CHANGE_FORM_NAME,
  ADD_FORM_ELEMENT_SETTINGS_BLOCK,
  DELETE_FORM_ELEMENT_SETTINGS_BLOCK,
  CHANGE_BLOCK_NAME,
  CHANGE_BLOCK_NECESSITY,
  ADD_OPTION,
  CHANGE_OPTION,
  DELETE_OPTION,
  FORM_NAME,
  BLOCK_NAME,
  MOVE_BLOCK_UP,
  MOVE_BLOCK_DOWN,
  DOWN,
  UP,
  ERRORS,
  DUPLICATE,
  EMPTY,
} from './constants';

export const initialState = fromJS({ [FORM_NAME]: '', [VALID]: true }).set(
  FORM_ELEMENTS,
  OrderedMap(),
);

function getInputWithErrorsData() {
  return {
    [VALUE]: '',
    [ERRORS]: {
      [EMPTY]: true,
      [DUPLICATE]: false,
    },
  };
}

function getImmutableOptionWithId() {
  const uniqueId = uuidv4();

  return [uniqueId, fromJS(getInputWithErrorsData())];
}

function moveBlockByStep(state, action, direction) {
  return state.update(FORM_ELEMENTS, formElements => {
    let firstCompare = true;

    return formElements.sortBy(
      (formElement, id) => id,
      (one, another) => {
        const idToCompare = direction === UP ? another : one;

        if (idToCompare === action.payload.blockId && firstCompare) {
          firstCompare = false;
          return 1;
        }

        return 0;
      },
    );
  });
}

function validateInputsGroup(state, { groupPath, valuePath, errorsPath }) {
  const duplicates = Array.from(
    state
      .getIn(groupPath)
      .groupBy(item => item.getIn(valuePath).trim())
      .filter(group => group.size > 1)
      .flatten(1)
      .keys(),
  );

  return state.updateIn(groupPath, group =>
    group.map((item, id) =>
      item
        .setIn([...errorsPath, DUPLICATE], duplicates.includes(id))
        .setIn([...errorsPath, EMPTY], item.getIn(valuePath).length === 0),
    ),
  );
}

function validateBlocks(state) {
  return validateInputsGroup(state, {
    groupPath: [FORM_ELEMENTS],
    valuePath: [BLOCK_NAME, VALUE],
    errorsPath: [BLOCK_NAME, ERRORS],
  });
}

function validateOptions(state, groupPath) {
  return validateInputsGroup(state, {
    groupPath,
    valuePath: [VALUE],
    errorsPath: [ERRORS],
  });
}

function inputIsValid(input) {
  return input.every(error => !error);
}

function validateForm(state) {
  const formIsValid = state.get(FORM_ELEMENTS).every(formElement => {
    let optionsAreValid = true;
    const options = formElement.get(OPTIONS);

    if (options) {
      optionsAreValid = options.every(option =>
        inputIsValid(option.get(ERRORS)),
      );
    }

    return (
      inputIsValid(formElement.getIn([BLOCK_NAME, ERRORS])) && optionsAreValid
    );
  });

  return state.set(VALID, formIsValid);
}

function formConstructorPageReducer(state = initialState, action) {
  const blockId = (action && action.payload && action.payload.blockId) || '';
  const pathToBlock = [FORM_ELEMENTS, blockId];
  const pathToOptions = [...pathToBlock, OPTIONS];
  const pathToRequired = [...pathToBlock, REQUIRED];

  switch (action.type) {
    case CHANGE_FORM_NAME:
      return state.set(FORM_NAME, action.payload.name);
    case ADD_FORM_ELEMENT_SETTINGS_BLOCK:
      return validateForm(
        validateBlocks(
          state.update(FORM_ELEMENTS, formElements => {
            const uniqueId = uuidv4();

            return formElements.set(
              uniqueId,
              fromJS({
                type: action.payload.type,
                [BLOCK_NAME]: getInputWithErrorsData(),
                [REQUIRED]: false,
                [OPTIONS]: action.payload.withOptions
                  ? OrderedMap([getImmutableOptionWithId()])
                  : null,
              }),
            );
          }),
        ),
      );
    case DELETE_FORM_ELEMENT_SETTINGS_BLOCK:
      return validateForm(
        validateBlocks(
          state.update(FORM_ELEMENTS, formElements =>
            formElements.delete(action.payload.id),
          ),
        ),
      );
    case CHANGE_BLOCK_NAME:
      return validateForm(
        validateBlocks(
          state.setIn([...pathToBlock, BLOCK_NAME, VALUE], action.payload.name),
        ),
      );
    case CHANGE_BLOCK_NECESSITY:
      return state.setIn(pathToRequired, !state.getIn(pathToRequired));
    case MOVE_BLOCK_UP:
      return moveBlockByStep(state, action, UP);
    case MOVE_BLOCK_DOWN:
      return moveBlockByStep(state, action, DOWN);
    case ADD_OPTION:
      return validateForm(
        validateOptions(
          state.updateIn(pathToOptions, options =>
            options.set(...getImmutableOptionWithId()),
          ),
          pathToOptions,
        ),
      );
    case CHANGE_OPTION:
      return validateForm(
        validateOptions(
          state.setIn(
            [...pathToOptions, action.payload.optionId, VALUE],
            action.payload.value,
          ),
          pathToOptions,
        ),
      );
    case DELETE_OPTION:
      return validateForm(
        validateOptions(
          state.updateIn(pathToOptions, options =>
            options.delete(action.payload.optionId),
          ),
          pathToOptions,
        ),
      );
    default:
      return state;
  }
}

export default formConstructorPageReducer;
