/*
 *
 * FormConstructorPage constants
 *
 */

export const CHANGE_FORM_NAME =
  'formConstructor/FormConstructorPage/CHANGE_FORM_NAME';

export const ADD_FORM_ELEMENT_SETTINGS_BLOCK =
  'formConstructor/FormConstructorPage/ADD_FORM_ELEMENT_SETTINGS_BLOCK';

export const DELETE_FORM_ELEMENT_SETTINGS_BLOCK =
  'formConstructor/FormConstructorPage/DELETE_FORM_ELEMENT_SETTINGS_BLOCK';

export const CHANGE_BLOCK_NAME =
  'formConstructor/FormConstructorPage/CHANGE_BLOCK_NAME';

export const CHANGE_BLOCK_NECESSITY =
  'formConstructor/FormConstructorPage/CHANGE_BLOCK_NECESSITY';

export const MOVE_BLOCK_UP =
  'formConstructor/FormConstructorPage/MOVE_BLOCK_UP';

export const MOVE_BLOCK_DOWN =
  'formConstructor/FormConstructorPage/MOVE_BLOCK_DOWN';

export const CHANGE_OPTION =
  'formConstructor/FormConstructorPage/CHANGE_OPTION';

export const ADD_OPTION = 'formConstructor/FormConstructorPage/ADD_OPTION';

export const DELETE_OPTION =
  'formConstructor/FormConstructorPage/DELETE_OPTION';

export const FORM_NAME = 'formName';

export const BLOCK_NAME = 'blockName';

export const UP = 'up';

export const DOWN = 'down';

export const ERRORS = 'errors';

export const DUPLICATE = 'duplicate';

export const EMPTY = 'empty';
