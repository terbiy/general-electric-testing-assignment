import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { TEXT_AREA, SELECT, CHECKBOX } from 'globalConstants';
import styled from 'styled-components';

import StylelessUl from 'components/StylelessUl';

import messages from './messages';

const AVAILABLE_ELEMENTS = [
  {
    type: 'text',
    withOptions: false,
  },
  {
    type: TEXT_AREA,
    withOptions: false,
  },
  {
    type: CHECKBOX,
    withOptions: true,
  },
  {
    type: 'radio',
    withOptions: true,
  },
  {
    type: SELECT,
    withOptions: true,
  },
  {
    type: 'file',
    withOptions: false,
  },
];

const Wrapper = styled.section`
  padding-top: 64px;
  flex-basis: 20%;
`;

const Button = styled.button`
  text-align: left;
  padding: 0;
`;

const AddTitle = styled.span`
  font-weight: bold;
`;

function getAddElementControl(options, addBlock) {
  const { type } = options;

  return (
    <li key={type}>
      <Button onClick={() => addBlock(options)}>
        <FormattedMessage {...messages[type]} />
      </Button>
    </li>
  );
}

function FormElementAddingBlock(props) {
  return (
    <Wrapper>
      <AddTitle>
        <FormattedMessage {...messages.add} />
      </AddTitle>
      <StylelessUl className="horizontal">
        {AVAILABLE_ELEMENTS.map(element =>
          getAddElementControl(element, props.onAddBlock),
        )}
      </StylelessUl>
    </Wrapper>
  );
}

FormElementAddingBlock.propTypes = {
  onAddBlock: PropTypes.func,
};

export default FormElementAddingBlock;
