import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import { sizes } from 'global-styles';

import Add from 'components/Add';
import Delete from 'components/Delete';
import Arrows from 'components/Arrows';
import FormBlockWrapper from 'components/FormBlockWrapper';
import TextInputWithErrors from 'components/TextInputWithErrors';

import messages from './messages';

function getOptions(options, onAdd, onChange, onDelete) {
  if (!options) {
    return null;
  }

  return (
    <div>
      {options.map(option =>
        getOption(option, onChange, onDelete, options.length > 1),
      )}
      <Add onAdd={onAdd} />
    </div>
  );
}

const OptionWrapper = styled.div`
  padding-right: 16px;
  position: relative;
`;

function getOption(option, onChange, onDelete, canDelete) {
  const {
    id,
    data: { value, errors },
  } = option;

  return (
    <OptionWrapper key={id}>
      <FormattedMessage {...messages.typeOption}>
        {typeOption => (
          <TextInputWithErrors
            id={id}
            value={value}
            errors={errors}
            onChange={event => onChange(id, event.target.value)}
            placeholder={typeOption}
          />
        )}
      </FormattedMessage>
      {getDeleteButton(canDelete, () => onDelete(id))}
    </OptionWrapper>
  );
}

function getDeleteButton(canDelete, onDelete) {
  if (canDelete) {
    return (
      <PlacedDelete>
        <Delete onDelete={onDelete} />
      </PlacedDelete>
    );
  }

  return null;
}

const PlacedDelete = styled.div`
  position: absolute;
  top: 0;
  right: 0;
`;

const ElementData = styled.div`
  position: relative;
  flex-basis: 95%;
  padding: 0 16px;
`;

const InfoAndRequired = styled.div`
  height: 24px;
  display: flex;
  justify-content: space-between;

  @media only screen and (max-width: ${sizes.small}) {
    font-size: 12px;
  }
`;

const RequiredMessage = styled.span`
  margin-left: 8px;
`;

const Info = styled.div`
  color: lightslategrey;
`;

function FormElementSettingsBlock(props) {
  const { data } = props;

  return (
    <FormBlockWrapper>
      <Arrows
        onMoveBlockUp={props.onMoveBlockUp}
        onMoveBlockDown={props.onMoveBlockDown}
      />
      <ElementData>
        <PlacedDelete>
          <Delete onDelete={props.onDeleteBlock} />
        </PlacedDelete>
        <FormattedMessage {...messages.typeBlockName}>
          {typeBlockName => (
            <TextInputWithErrors
              value={data.blockName.value}
              errors={data.blockName.errors}
              placeholder={typeBlockName}
              big
              onChange={props.onChangeBlockName}
            />
          )}
        </FormattedMessage>
        {getOptions(
          data.options,
          props.onAddOption,
          props.onChangeOption,
          props.onDeleteOption,
        )}
        <InfoAndRequired>
          <Info>
            <FormattedMessage {...messages[data.type]} />
          </Info>
          <label>
            <input
              type="checkbox"
              value={data.required}
              checked={data.required}
              onChange={props.onChangeBlockNecessity}
            />
            <RequiredMessage>
              <FormattedMessage {...messages.makeRequired} />
            </RequiredMessage>
          </label>
        </InfoAndRequired>
      </ElementData>
    </FormBlockWrapper>
  );
}

const InputWithErrorsPropType = PropTypes.shape({
  value: PropTypes.string,
  errors: PropTypes.shape({
    empty: PropTypes.bool,
    duplicate: PropTypes.bool,
  }),
});

FormElementSettingsBlock.propTypes = {
  data: PropTypes.shape({
    type: PropTypes.string,
    blockName: InputWithErrorsPropType,
    required: PropTypes.bool,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string,
        data: InputWithErrorsPropType,
      }),
    ),
  }),
  onMoveBlockUp: PropTypes.func,
  onMoveBlockDown: PropTypes.func,
  onChangeBlockName: PropTypes.func,
  onChangeBlockNecessity: PropTypes.func,
  onDeleteBlock: PropTypes.func,
  onAddOption: PropTypes.func,
  onChangeOption: PropTypes.func,
  onDeleteOption: PropTypes.func,
};

export default FormElementSettingsBlock;
