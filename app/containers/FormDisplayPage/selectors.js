import { createSelector } from 'reselect';
import { FORM_ELEMENTS, VALID } from 'globalConstants';
import { getArrayFormOrderMapWithOptions } from 'utils/customUtils';
import { initialState } from './reducer';
import {
  selectFormElements as selectFormElementsInitialData,
  selectFormName,
} from '../FormConstructorPage/selectors';
import { SUBMIT_CONFIRMED } from './constants';

const selectFormDisplayState = state =>
  state.get('formDisplayPage', initialState);

const selectFormElements = createSelector(
  selectFormDisplayState,
  formDisplayState =>
    getArrayFormOrderMapWithOptions(formDisplayState.get(FORM_ELEMENTS)),
);

const selectSubmitConfirmed = createSelector(
  selectFormDisplayState,
  formDisplayState => formDisplayState.get(SUBMIT_CONFIRMED),
);

const selectFormValidity = createSelector(
  selectFormDisplayState,
  formDisplayState => formDisplayState.get(VALID),
);

export {
  selectFormDisplayState,
  selectFormElementsInitialData,
  selectFormName,
  selectFormValidity,
  selectFormElements,
  selectSubmitConfirmed,
};
