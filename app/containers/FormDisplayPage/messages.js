/*
 * FormDisplayPage Messages
 *
 * This contains all the text for the FormDisplayPage component.
 */

import { defineMessages } from 'react-intl';

export default defineMessages({
  submitForm: {
    id: 'formConstructor.containers.FormDisplayPage.submitForm',
    defaultMessage: 'Отправить форму',
  },
  confirmSubmit: {
    id: 'formConstructor.containers.FormDisplayPage.confirmSubmit',
    defaultMessage: 'Форма отправлена',
  },
  noOptionSelected: {
    id: 'formConstructor.containers.FormDisplayPage.noOptionSelected',
    defaultMessage: '— Не выбрано —',
  },
  errorEmpty: {
    id: 'formConstructor.containers.FormDisplayPage.errorEmpty',
    defaultMessage: 'Обязательно к заполнению',
  },
});
