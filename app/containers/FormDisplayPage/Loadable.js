/**
 * Asynchronously loads the component for FormDisplayPage
 */
import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
