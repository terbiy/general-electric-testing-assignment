/*
 *
 * FormDisplayPage reducer
 *
 */

import { fromJS, OrderedMap, Set as ImmutableSet } from 'immutable';
import {
  FORM_ELEMENTS,
  VALID,
  VALUE,
  CHECKBOX,
  REQUIRED,
} from 'globalConstants';
import {
  INITIALIZE_FORM_DATA,
  RESET_FORM_DATA,
  SUBMIT_CONFIRMED,
  SUBMIT_FORM,
  USE_INPUT,
} from './constants';

export const initialState = fromJS({
  [VALID]: true,
  [SUBMIT_CONFIRMED]: false,
}).set(FORM_ELEMENTS, OrderedMap());

function validateInputs(state) {
  return state.update(FORM_ELEMENTS, formElements =>
    formElements.map(formElement => {
      if (formElement.get(REQUIRED)) {
        const value = formElement.get(VALUE);
        const valid = !(value === '' || value.size === 0);

        return formElement.set(VALID, valid);
      }

      return formElement.set(VALID, true);
    }),
  );
}

function validateForm(state) {
  return state.set(
    VALID,
    state.get(FORM_ELEMENTS).every(formElement => formElement.get(VALID)),
  );
}

function validateInputsAndForm(state) {
  return validateForm(validateInputs(state));
}

function formDisplayPageReducer(state = initialState, action) {
  switch (action.type) {
    case INITIALIZE_FORM_DATA:
      return validateInputsAndForm(
        state.update(FORM_ELEMENTS, formElements =>
          action.payload.formElementsData.reduce(
            (fillingFormElements, formElementData) => {
              const {
                id,
                data: {
                  type,
                  blockName: { value: name },
                  options,
                  required,
                },
              } = formElementData;

              const transformedOptions =
                options &&
                OrderedMap(
                  options.map(option => [option.id, option.data.value]),
                );

              const value = type === CHECKBOX ? ImmutableSet() : '';

              return fillingFormElements.set(
                id,
                fromJS({
                  type,
                  name,
                  options: transformedOptions,
                  required,
                  [VALID]: true,
                  value,
                }),
              );
            },
            formElements,
          ),
        ),
      );
    case RESET_FORM_DATA:
      return initialState;
    case USE_INPUT:
      return validateInputsAndForm(
        state.update(FORM_ELEMENTS, formElements => {
          const { id, value } = action.payload;
          const pathToValue = [id, VALUE];

          if (ImmutableSet.isSet(formElements.getIn(pathToValue))) {
            return formElements.updateIn(pathToValue, valuesSet => {
              if (valuesSet.includes(value)) {
                return valuesSet.delete(value);
              }

              return valuesSet.add(value);
            });
          }

          return formElements.setIn(pathToValue, value);
        }),
      );
    case SUBMIT_FORM:
      return state.set(SUBMIT_CONFIRMED, true);
    default:
      return state;
  }
}

export default formDisplayPageReducer;
