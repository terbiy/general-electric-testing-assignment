/*
 *
 * FormDisplayPage actions
 *
 */

import {
  INITIALIZE_FORM_DATA,
  RESET_FORM_DATA,
  SUBMIT_FORM,
  USE_INPUT,
} from './constants';

export function initializeFormData(formElementsData) {
  return {
    type: INITIALIZE_FORM_DATA,
    payload: {
      formElementsData,
    },
  };
}

export function resetFormData() {
  return {
    type: RESET_FORM_DATA,
  };
}

export function useInput(id, value) {
  return {
    type: USE_INPUT,
    payload: {
      id,
      value,
    },
  };
}

export function submitForm() {
  return {
    type: SUBMIT_FORM,
  };
}
