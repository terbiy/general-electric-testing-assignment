/*
 *
 * FormDisplayPage constants
 *
 */

export const INITIALIZE_FORM_DATA =
  'formConstructor/FormDisplayPage/INITIALIZE_FORM_DATA';

export const RESET_FORM_DATA =
  'formConstructor/FormDisplayPage/RESET_FORM_DATA';

export const USE_INPUT = 'formConstructor/FormDisplayPage/USE_INPUT';

export const SUBMIT_FORM = 'formConstructor/FormDisplayPage/SUBMIT_FORM';

export const SUBMIT_CONFIRMED = 'submitConfirmed';
