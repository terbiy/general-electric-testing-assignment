/**
 *
 * FormDisplayPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import injectReducer from 'utils/injectReducer';
import { push } from 'react-router-redux';
import styled from 'styled-components/';

import { FORM_CONSTRUCTOR_ROUTE, TEXT_AREA, SELECT } from 'globalConstants';
import Error from 'components/Error';
import FormBlockWrapper from 'components/FormBlockWrapper';
import H1 from 'components/H1';
import H2 from 'components/H2';
import TextInput from 'components/TextInput';
import TextArea from 'components/TextArea';
import StylelessUl from 'components/StylelessUl';
import StyledButton from 'components/StyledButton';

import {
  selectFormElementsInitialData,
  selectFormName,
  selectFormValidity,
  selectFormElements,
  selectSubmitConfirmed,
} from './selectors';
import reducer from './reducer';
import messages from './messages';
import FormEditButton from '../../components/FormEditButton';
import {
  initializeFormData,
  resetFormData,
  submitForm,
  useInput,
} from './actions';

const FormBlockInnerWrapper = styled.div`
  width: 90%;
  margin: 0 5%;
`;

const InputWithMargin = styled.input`
  margin-right: 8px;
`;

const FormWrapper = styled.div`
  margin-bottom: 2em;
`;

/* eslint-disable react/prefer-stateless-function */
export class FormDisplayPage extends React.PureComponent {
  constructor(props) {
    super(props);

    props.onInitializeFormData(props.formElementsInitialData);
  }

  render() {
    const buttonMessage = this.props.submitConfirmed
      ? messages.confirmSubmit
      : messages.submitForm;
    const buttonAction = this.props.submitConfirmed
      ? () => {}
      : this.props.onSubmitForm;

    return (
      <FormWrapper>
        <FormEditButton onFormEdit={this.props.onEditForm} />
        <form onSubmit={this.props.onSubmitForm}>
          <H1>{this.props.formName}</H1>
          {this.getElements()}
          <FormattedMessage {...buttonMessage}>
            {submitFormMessage => (
              <StyledButton
                type="submit"
                className="submit"
                disabled={!this.props.formIsValid}
                onClick={buttonAction}
              >
                {submitFormMessage}
              </StyledButton>
            )}
          </FormattedMessage>
        </form>
      </FormWrapper>
    );
  }

  componentWillUnmount() {
    this.props.onFormUnmount();
  }

  getElements() {
    return this.props.formElements.map(elementData =>
      FormDisplayPage.getElement(elementData, this.props.makeOnUseInput),
    );
  }

  static getElement(element, makeOnUseInput) {
    const { id, data } = element;

    const onUseInput = makeOnUseInput(id);

    return (
      <FormBlockWrapper key={id}>
        <FormBlockInnerWrapper>
          <H2>{data.name}</H2>
          <div>
            {FormDisplayPage.getInput(data, onUseInput)}
            {this.getError(data)}
          </div>
        </FormBlockInnerWrapper>
      </FormBlockWrapper>
    );
  }

  static getError(data) {
    if (data.valid) {
      return null;
    }

    return (
      <Error>
        <FormattedMessage {...messages.errorEmpty} />
      </Error>
    );
  }

  static getInput(data, onUseInput) {
    const { type, options, value } = data;
    if (type === SELECT) {
      return (
        <select value={value} onChange={onUseInput}>
          <FormattedMessage {...messages.noOptionSelected}>
            {noOptionSelected => (
              <option value="default">{noOptionSelected}</option>
            )}
          </FormattedMessage>
          {options.map(FormDisplayPage.getSelectOption)}
        </select>
      );
    }

    if (options && options.length > 0) {
      return (
        <StylelessUl>
          {options.map(option => {
            const { data: optionData } = option;

            const checked =
              value === optionData ||
              (Array.isArray(value) && value.includes(optionData));

            return FormDisplayPage.getOption(
              { ...option, type, checked },
              onUseInput,
            );
          })}
        </StylelessUl>
      );
    }

    if (type === TEXT_AREA) {
      return <TextArea value={value} onChange={onUseInput} />;
    }

    return (
      <TextInput
        className="emphasis"
        type={type}
        value={value}
        onChange={onUseInput}
      />
    );
  }

  static getSelectOption(option) {
    const { id, data: value } = option;

    return (
      <option key={id} value={value}>
        {value}
      </option>
    );
  }

  static getOption(option, onUseInput) {
    const { id, data: label, type, checked } = option;

    return (
      <li key={id}>
        <label>
          <InputWithMargin
            type={type}
            checked={checked}
            value={label}
            onChange={onUseInput}
          />
          {label}
        </label>
      </li>
    );
  }
}

FormDisplayPage.propTypes = {
  formName: PropTypes.string,
  formIsValid: PropTypes.bool,
  formElementsInitialData: PropTypes.array,
  formElements: PropTypes.array,
  onInitializeFormData: PropTypes.func,
  onSubmitForm: PropTypes.func,
  submitConfirmed: PropTypes.bool,
  onEditForm: PropTypes.func,
  makeOnUseInput: PropTypes.func,
  onFormUnmount: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  formName: selectFormName,
  formIsValid: selectFormValidity,
  formElementsInitialData: selectFormElementsInitialData,
  formElements: selectFormElements,
  submitConfirmed: selectSubmitConfirmed,
});

function mapDispatchToProps(dispatch) {
  return {
    onInitializeFormData: formElementsData =>
      dispatch(initializeFormData(formElementsData)),
    onSubmitForm: event => {
      event.preventDefault();

      dispatch(submitForm());
    },
    onEditForm: () => dispatch(push(FORM_CONSTRUCTOR_ROUTE)),
    makeOnUseInput: id => event => dispatch(useInput(id, event.target.value)),
    onFormUnmount: () => dispatch(resetFormData()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'formDisplayPage', reducer });

export default compose(
  withReducer,
  withConnect,
)(FormDisplayPage);
