export const FORM_CONSTRUCTOR_ROUTE = '/';

export const FORM_DISPLAY_ROUTE = '/form-display';

export const TEXT_AREA = 'textarea';

export const SELECT = 'select';

export const CHECKBOX = 'checkbox';

export const FORM_ELEMENTS = 'formElements';

export const VALID = 'valid';

export const OPTIONS = 'options';

export const VALUE = 'value';

export const REQUIRED = 'required';
